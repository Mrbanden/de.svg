'use strict'

const $ = function(foo) {

    return document.getElementById(foo);
}



let svg = document.getElementsByTagName('svg')[0];
let width = 100;
svg.setAttribute("width", width + '%');


let realwidth = svg.clientWidth;
let height = (realwidth / 3) *2;
svg.setAttribute("height", height);


let squarey = 0;
let recty = 0;

for (let i = 0; i < 2; i++) {
	let square = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
	square.setAttribute("fill","white");
	square.setAttribute("width", realwidth);
	square.setAttribute("height", height/2);	
	if (i = 2) {
	square.setAttribute("fill","red");
	square.setAttribute("y", height/2);
	}
	svg.appendChild(square);	
}

let circle = document.createElementNS("http://www.w3.org/2000/svg", 'circle');
circle.setAttribute("fill","red");
circle.setAttribute("cy", height/2);
circle.setAttribute("cx", height/2);
circle.setAttribute("r", realwidth/4);

svg.appendChild(circle);	

let path = document.createElementNS("http://www.w3.org/2000/svg", 'path');
path.setAttribute("fill","white");
path.setAttribute("d", " M "+ realwidth/12 +"," + height/2 +" a1,1 0 0,0 " +height/(1+1/3) +",0");

svg.appendChild(path);	
