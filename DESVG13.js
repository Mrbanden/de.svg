'use strict'

const $ = function(foo) {

    return document.getElementById(foo);
}



let svg = document.getElementsByTagName('svg')[0];
let width = 100;
svg.setAttribute("width", width + '%');


let realwidth = svg.clientWidth;
let height = (realwidth / 19) *10;
svg.setAttribute("height", height);


let stripey = 0;
for (let i = 0; i < 7; i++) {
let stripe = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
let stripeh = height / 13;

stripe.setAttribute("fill","red");
stripe.setAttribute("width", realwidth);
stripe.setAttribute("height", stripeh);
stripe.setAttribute("y", stripey);
stripey = stripey + (2*stripeh);

svg.appendChild(stripe);

}

let starfield = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
let starfieldy = (height / 13)*7;
let starfieldx = (realwidth/5)*2;
starfield.setAttribute("fill","blue");
starfield.setAttribute("width", starfieldx);
starfield.setAttribute("height", starfieldy);
svg.appendChild(starfield);



let gridx = (starfieldx/24);
let gridy = (starfieldy/20);

let stary = gridy;
let starx = gridx;

const drawstar = function() {
	let star = document.createElementNS("http://www.w3.org/2000/svg", 'polygon');
	star.setAttribute("points","10,1 4,19.8 19,7.8 1,7.8 16,19.8");
	star.setAttribute("fill","white");
	star.setAttribute("cx","48");
	star.setAttribute("transform","translate(" + starx +"," +stary+ ")scale(" + gridx /10 +"," + gridy /10 + ")" );
	svg.appendChild(star);
}

const starmaker = function() {
	for (let i = 0; i < 6; i++) {

		for (let j = 0; j < 5; j++) {
			drawstar();
			stary = stary + gridy*4;

		}
		stary = gridy;
		starx = starx + gridx*4;
	}
	starx = gridx*3;

	for (let i = 0; i < 5; i++) {
		stary = gridy*3;
		for (let j = 0; j < 4; j++) {
			drawstar();
			stary = stary + gridy*4;

		}
		stary = gridy;
		starx = starx + gridx*4;
	}
		
}

starmaker();