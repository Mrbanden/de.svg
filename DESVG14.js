"use strict"
export const $ = function (foo) {return document.getElementById(foo);}


/*
 * animate svg, params:
 * svgelm: id of svg in html
 * widx: width of animated svg
 * wp: viewport (html elm) holding the svgelm
 */

export const anim = function (svgelm, widx, dx, wp, dy) {
    let vb = window.getComputedStyle($(wp));
    let xmax = parseInt(vb.getPropertyValue('width'));

    let timer = setInterval(function () {
        let svg = window.getComputedStyle($(svgelm));
        let left = parseInt(svg.getPropertyValue('left'));
  		let top = parseInt(svg.getPropertyValue('top'));
  		

  		

        if (left < 0 || left + widx > xmax) {

            dx *= -1;
            dy = 0;
        } 

        else if (top > 0  || left + widx > xmax) {
        
		dx *= 0;
		dy *= -1;
        }

        $(svgelm).style.top  = top + dy + 'px';
        $(svgelm).style.left  = left + dx + 'px';
  		

 
    }, 10);

}