'use strict'

const $ = function(foo) {

    return document.getElementById(foo);
}



let svg = document.getElementsByTagName('svg')[0];
let width = 100;
svg.setAttribute("width", width + '%');


let realwidth = svg.clientWidth;
let height = (realwidth / 17) *14;
svg.setAttribute("height", height);


let squarey = 0;
let recty = 0;
let squareh = height / 3;
let squarew = height /3;
let squarex = 0;
for (let i = 0; i < 4; i++) {

	let square = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
	square.setAttribute("fill","red");
	
	if (i < 2) {
	square.setAttribute("width", squarew);
	square.setAttribute("height", squareh);
	
	square.setAttribute("y", squarey*1.5);
	squarey = squarey + squareh;
	svg.appendChild(square);
	}

	else if (i >= 2) {

	square.setAttribute("width", realwidth/1.5);
	square.setAttribute("height", squareh);
	square.setAttribute("x", squarex+(squarew*1.5));
	
	square.setAttribute("y", recty*1.5);
	recty = recty + squareh;
	svg.appendChild(square);
	}
	
}

